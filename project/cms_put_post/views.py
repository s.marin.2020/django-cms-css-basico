import random

from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse
from .models import Contenido
from django.views.decorators.csrf import csrf_exempt
from random import randint


@csrf_exempt
def get_content(request, llave):
    if request.method == "PUT":
        valor = request.body.decode('utf-8')
        c = Contenido.objects.update_or_create(
            clave=llave,
            defaults={'valor': valor}
        )
    contenido = get_object_or_404(Contenido, clave=llave)
    return render(request, 'cms_put_post/content.html', {'content': contenido})


def index(request):
    content_list = Contenido.objects.all()
    context = {'content': content_list}
    return render(request, 'cms_put_post/index.html', context)


def style(self):
    colores = ['blue', 'red', 'gold', 'aqua', 'green', 'black', 'yellow']
    random.seed()
    n = randint(0, 6)
    color = colores[n]
    n = randint(0, 6)
    back = colores[n]
    regla = """body {
margin: 10px 20% 50px 70px;
font-family: sans-serif;
"""
    regla += f"color:{color};\nbackground:{back};"
    regla += "}"
    return HttpResponse(regla, content_type="text/css")
