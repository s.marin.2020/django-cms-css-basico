from django.urls import path
from . import views

urlpatterns = [
    path("", views.index),
    path('main.css', views.style),
    path("<str:llave>", views.get_content, name="get_content")
]